alias aliases='compgen -a'
alias l='ls -alh'
alias ll='ls -lh'
alias free="free -m"
alias showspace='du --human-readable --total . | sort -r --human-numeric-sort | head -n 20'
alias rm_svndir='find . -name .svn -print0 | xargs -0 rm -rf'
alias rm_gitdir='find . -name .git* -print0 | xargs -0 rm -rf'
alias mem='$HOME/bin/memusage'

# allow for loccal overrides
[ -f "~/.bash_aliases.local" ] && source "~/.bash_aliases.local"